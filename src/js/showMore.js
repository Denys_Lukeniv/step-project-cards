document.addEventListener('click',(e)=>{
    e.preventDefault()
    if (e.target.classList.contains('visit-card__showMore')){
       let cardWrapper =  e.target.closest(".visit-card")
       let children = [...cardWrapper.children]
       children.forEach((child)=>{
            child.style.display ='block'
    })
    e.target.style.display = 'none'
}   
    
})

