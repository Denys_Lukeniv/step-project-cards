// файл содержит утилиты для фильтрации по трем критериям: по полям status, urgency, description/purpose

function filterByStatus(arr, val){
    return filterByProperty(arr, 'status', val);
}

function filterByUrgency(arr,val){
    return filterByProperty(arr, 'urgency', val);
}

function filterByProperty(arr,prop,valProp){
    if (!valProp) return arr; // if val is empty then return all arr items
    return arr.filter(item =>{
        return item[prop].toLowerCase() === valProp.toLowerCase()
    })   
}

function filterBySearchText(arr, str){
    if (!str) return arr;
    str = str.toLowerCase().trim() 
    return arr.filter(item=>{
        return  (item.purpose.toLowerCase().indexOf(str) !==-1) ||
        (item.description.toLowerCase().indexOf(str) !== -1)
    })
}

function filterAllForm(arr, statusVal, urgencyVal, strVal){
    let resArray = [...arr];
    resArray = filterByUrgency(resArray,urgencyVal) 
    resArray = filterByStatus(resArray, statusVal) 
    resArray = filterBySearchText(resArray, strVal)
    return resArray
}