import Visit from './Visit.js';

class VisitTherapist extends Visit{
  constructor(purpose, description, urgency, fullName, age, status){
    super(purpose, description, urgency, fullName, status);
    this.age = age;
  }

  setAge(age){
    this.age = age;
  }

  getAge(){
    return this.age;
  }
  render(){
    return `
    <button class="visit-card__close">X</button>
    <p class="visit-card__fullname">FullName: ${this.fullName}</p>
    <p class="visit-card__doctor">Doctor: Therapist</p>
    <button class="visit-card__showMore">Show more</button>
    <button class="visit-card__change">Change</button>
`
  }
  getDataJson(){

    return { 
      purpose: this.purpose,
      description: this.description,
      urgency: this.urgency,
      fullName: this.fullName,
      age: this.age,
      status: this.status,
      doctor: 'therapist'
    };
  }
}
export default VisitTherapist;
