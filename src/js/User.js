export default class User {
    static URL = "https://ajax.test-danit.com/api/v2/cards";

    static getHeaders() {
        return {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.token}`
        }
    }

    static async login(emailInput, passwordInput) {
        return await fetch(`${User.URL}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: emailInput, password: passwordInput })
        }).then( async res => {
            if (res.ok) {
                localStorage.setItem('token', await res.text());
                return true;
            }
            return false;
        });
    };
}
