
class Modal {
    constructor() {
    }
    setDoctor(doctor) {
        this.doctor = doctor
    }
    getDoctor() {
        return this.doctor
    }
    renderNewFields() {
        //wrapper//
        const createVisitModal = document.querySelector('.visitModal')
        if (document.querySelector('.activeInputs')) document.querySelector('.activeInputs').remove()
        if (this.doctor !== 'none') {

            //new general modal-body elements//
            const newElemsWrapper = document.createElement("div")
            newElemsWrapper.classList.add("activeInputs")

            let labelPurpose = document.createElement('label');
            labelPurpose.setAttribute('for', 'purpose');
            labelPurpose.innerHTML = 'Purpose:'
            labelPurpose.setAttribute('class', 'modal-label');
            const purposeInput = document.createElement("input")
            purposeInput.id = "purpose"
            purposeInput.setAttribute('class', 'modal-input-style');
            const purposeError = document.createElement("div")
            purposeError.setAttribute('class', 'error');
            purposeError.id = 'purposeError'
            purposeError.innerHTML = 'This field must not be blank'
            purposeError.style.display = 'none'

            let labelVisitDescription = document.createElement('label');
            labelVisitDescription.setAttribute('for', 'visitDescription');
            labelVisitDescription.innerHTML = 'Visit description:'
            labelVisitDescription.setAttribute('class', 'modal-label');
            const visitDescription = document.createElement("input")
            visitDescription.id = 'visitDescription'
            visitDescription.setAttribute('class', 'modal-input-style');
            const visitDescriptionError = document.createElement("div")
            visitDescriptionError.setAttribute('class', 'error');
            visitDescriptionError.id = 'visitDescriptionError'
            visitDescriptionError.innerHTML = 'This field must not be blank'
            visitDescriptionError.style.display = 'none'

            let labelVisitStatus = document.createElement('label');
            labelVisitStatus.setAttribute('for', 'visitStatus');
            labelVisitStatus.innerHTML = 'Status:'
            labelVisitStatus.setAttribute('class', 'modal-label');
            const visitStatus = document.createElement("select")
            let optionNoneStatus = document.createElement("option");
            // optionNoneStatus.text = "Select status";
            // visitStatus.add(optionNoneStatus)
            let optionOpen = document.createElement("option");
            optionOpen.text = "Open";
            visitStatus.add(optionOpen)
            let optionDone = document.createElement("option");
            optionDone.text = "Done";
            visitStatus.add(optionDone)
            visitStatus.setAttribute('class', 'modal-select');
            visitStatus.id = 'visitStatus'






            let labelVisitUrgency = document.createElement('label');
            labelVisitUrgency.setAttribute('for', 'visitUrgency');
            labelVisitUrgency.innerHTML = 'Urgency visit:'
            labelVisitUrgency.setAttribute('class', 'modal-label');
            const visitUrgency = document.createElement("select")
            let optionNone = document.createElement("option");
            optionNone.text = "Select urgency";
            visitUrgency.add(optionNone)
            let optionHigh = document.createElement("option");
            optionHigh.text = "High";
            visitUrgency.add(optionHigh)
            let optionNormal = document.createElement("option");
            optionNormal.text = "Normal";
            visitUrgency.add(optionNormal)
            let optionLow = document.createElement("option");
            optionLow.text = "Low";
            visitUrgency.add(optionLow)
            visitUrgency.setAttribute('class', 'modal-select');
            visitUrgency.id = 'visitUrgency'
            const visitUrgencyError = document.createElement("div")
            visitUrgencyError.setAttribute('class', 'error');
            visitUrgencyError.id = 'visitUrgencyError'
            visitUrgencyError.innerHTML = 'This field must not be blank'
            visitUrgencyError.style.display = 'none'

            let labelFullName = document.createElement('label');
            labelFullName.setAttribute('for', 'fullName');
            labelFullName.innerHTML = 'Full name:'
            labelFullName.setAttribute('class', 'modal-label');
            const fullName = document.createElement("input")
            fullName.id = 'fullName'
            fullName.setAttribute('class', 'modal-input-style');
            const fullNameError = document.createElement("div")
            fullNameError.setAttribute('class', 'error');
            fullNameError.id = 'fullNameError'
            fullNameError.innerHTML = 'This field must not be blank'
            fullNameError.style.display = 'none'

            newElemsWrapper.append(labelPurpose, purposeInput, purposeError, labelVisitDescription, visitDescription, visitDescriptionError, labelVisitUrgency, visitUrgency,labelVisitStatus,visitStatus, visitUrgencyError, labelFullName, fullName, fullNameError)
            //new modal-body elements for cardiolog//

           
            if (this.doctor === "cardiolog") {
                let labelNormalPressure = document.createElement('label');
                labelNormalPressure.setAttribute('for', 'normalPressure');
                labelNormalPressure.innerHTML = 'Normal pressure:'
                labelNormalPressure.setAttribute('class', 'modal-label');
                const normalPressure = document.createElement("input")
                normalPressure.id = 'normalPressure'
                normalPressure.setAttribute('class', 'modal-input-style');
                const normalPressureError = document.createElement("div")
                normalPressureError.setAttribute('class', 'error');
                normalPressureError.id = 'normalPressureError'
                normalPressureError.innerHTML = 'This field must not be blank'
                normalPressureError.style.display = 'none'

                let labelMassIndex = document.createElement('label');
                labelMassIndex.setAttribute('for', 'massIndex');
                labelMassIndex.innerHTML = 'Mass index:'
                labelMassIndex.setAttribute('class', 'modal-label');
                const massIndex = document.createElement("input")
                massIndex.id = 'massIndex'
                massIndex.setAttribute('class', 'modal-input-style');
                const massIndexEror = document.createElement("div")
                massIndexEror.setAttribute('class', 'error');
                massIndexEror.id = 'massIndexEror'
                massIndexEror.innerHTML = 'This field must not be blank'
                massIndexEror.style.display = 'none'

                let labelHeartIllness = document.createElement('label');
                labelHeartIllness.setAttribute('for', 'heartIllness');
                labelHeartIllness.innerHTML = 'Heart illnes:'
                labelHeartIllness.setAttribute('class', 'modal-label');
                const heartIllness = document.createElement("input")
                heartIllness.id = 'heartIllness'
                heartIllness.setAttribute('class', 'modal-input-style');
                const heartIllnessError = document.createElement("div")
                heartIllnessError.setAttribute('class', 'error');
                heartIllnessError.id = 'heartIllnessError'
                heartIllnessError.innerHTML = 'This field must not be blank'
                heartIllnessError.style.display = 'none'

                let labelAge = document.createElement('label');
                labelAge.setAttribute('for', 'age');
                labelAge.innerHTML = 'Age:'
                labelAge.setAttribute('class', 'modal-label');
                const age = document.createElement("input")
                age.type = 'number'
                age.id = 'age'
                age.setAttribute('class', 'modal-input-style');
                const ageError = document.createElement("div")
                ageError.setAttribute('class', 'error');
                ageError.id = 'ageError'
                ageError.innerHTML = 'This field must not be blank'
                ageError.style.display = 'none'

                newElemsWrapper.append(labelNormalPressure, normalPressure, normalPressureError, labelMassIndex, massIndex, massIndexEror, labelHeartIllness, heartIllness, heartIllnessError, labelAge, age, ageError)
            } else if (this.doctor === "dentist") {
                let labelLastVisitDate = document.createElement('label');
                labelLastVisitDate.setAttribute('for', 'lastVisitDate');
                labelLastVisitDate.innerHTML = 'Age:'
                labelLastVisitDate.setAttribute('class', 'modal-label');
                const lastVisitDate = document.createElement("input")
                lastVisitDate.type = 'date'
                lastVisitDate.id = 'lastVisitDate'
                lastVisitDate.setAttribute('class', 'modal-input-style');
                const lastVisitDateError = document.createElement("div")
                lastVisitDateError.setAttribute('class', 'error');
                lastVisitDateError.id = 'lastVisitDateError'
                lastVisitDateError.innerHTML = 'This field must not be blank'
                lastVisitDateError.style.display = 'none'

                newElemsWrapper.append(labelLastVisitDate, lastVisitDate, lastVisitDateError)
            } else if (this.doctor === "therapist") {
                let labelAge = document.createElement('label');
                labelAge.setAttribute('for', 'age');
                labelAge.innerHTML = 'Age:'
                labelAge.setAttribute('class', 'modal-label');
                const age = document.createElement("input")
                age.type = 'number'
                age.id = 'age'
                age.setAttribute('class', 'modal-input-style');
                const ageError = document.createElement("div")
                ageError.setAttribute('class', 'error');
                ageError.innerHTML = 'This field must not be blank'
                ageError.style.display = 'none'
                ageError.id = 'ageError'
                newElemsWrapper.append(labelAge, age, ageError)

            }
            createVisitModal.append(newElemsWrapper)
        }
    }
}

export default Modal