class Visit {
    constructor(purpose, description, urgency, fullName,status) {
      this.purpose = purpose;
      this.description = description;
      this.urgency = urgency;
      this.fullName = fullName;
      this.status = status;
    }
  
  
    setPurpose(purpose) {
      this.purpose = purpose;
    }
    getPurpose() {
      return this.purpose;
    }
    setDescription(description) {
      this.description = description;
    }
    getDescription() {
      return this.description;
    }
    setUrgency(urgency) {
      this.urgency = urgency;
    }
    getUrgency() {
      return this.urgency;
    }
    setFullName(fullName) {
      this.fullName = fullName;
    }
    getFullName() {
      return this.fullName;
    }
    setStatus(status) {
      this.status = status;
    }
    getStatus() {
      return this.status;
    }
  
    getDataJson(){
      return "{}";
    }
  
  }
  
  export default Visit;
  