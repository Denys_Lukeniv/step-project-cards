import Visit from './Visit.js';
class VisitCardiolog extends Visit {
    constructor(purpose, description,urgency, fullName, pressure, massIndex, heartIllness, age, status) {
        super(purpose, description,urgency, fullName, status);
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.heartIllness = heartIllness;
        this.age = age;
    }

    setPressure(pressure) {
        this.pressure = pressure;
    }
    getPressure() {
        return this.pressure;
    }
    setMassIndex(massIndex) {
        this.massIndex = massIndex;
    }
    getMassIndex() {
        return this.massIndex;
    }
    setHeartIllness(heartIllness) {
        this.heartIllness = heartIllness;
    }
    getHeartIllness() {
        return this.heartIllness;
    }
    setAge(age) {
        this.age = age;
    }
    getAge() {
        return this.age;
    }
    render(){
        return `
        <button class="visit-card__close">X</button>
        <p class="visit-card__fullname">FullName: ${this.fullName}</p>
        <p class="visit-card__doctor">Doctor: Cardiolog</p>
        <button class="visit-card__showMore">Show more</button>
        <button class="visit-card__change">Change</button>
    `
    }
    getDataJson() {
        return {
            purpose: this.purpose,
            description: this.description,
            urgency: this.urgency,
            fullName: this.fullName,
            pressure: this.pressure,
            lastVisitDate: this.lastVisitDate,
            massIndex: this.massIndex,
            heartIllness: this.heartIllness,
            age: this.age,
            status: this.status,
            doctor: 'сardiolog'
        };

    }
}

export default VisitCardiolog;
