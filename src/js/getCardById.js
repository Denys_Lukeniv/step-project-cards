const dataBaseURL = 'https://ajax.test-danit.com/api/v2/cards/'
const getCardById = async function(id){
    return await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`,{
        method:"GET",
        headers:{
            'Authorization': `Bearer ${localStorage.token}`
        }
    })

}


export default getCardById