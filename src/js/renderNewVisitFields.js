import Modal from './Modal.js'

const modalSelectDoctor = document.getElementById('selectDoctor')

modalSelectDoctor.addEventListener('change',function(e){

    const visitRegistration = new Modal
    const doctor = e.target.value
    visitRegistration.setDoctor(doctor)
    visitRegistration.renderNewFields()
})