import Visit from "./Visit.js";
import VisitCardiolog from "./VisitCardiolog.js";
import VisitDentist from "./VisitDentist.js";
import VisitTherapist from "./VisitTherapist.js";
import User from "./User.js";
const cardsBox = document.querySelector('.cards-box')
let allCardsData

const getVisits = async function(){ 
    cardsBox.innerHTML =''
    const allVisits = await fetch('https://ajax.test-danit.com/api/v2/cards',{
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${localStorage.token}`
    }
}).then(response => response.json())
   allCardsData = allVisits.forEach(elem=>{
    let newCard = document.createElement('div')
    newCard.setAttribute('data-card-id',`${elem.id}`)
    newCard.setAttribute('class','visit-card')
    if (elem.dataBody.doctor ==='сardiolog'){newCard.innerHTML =` 
    <button class="visit-card__close">X</button>
    <p class="visit-card__fullname">FullName:${elem.dataBody.fullName}</p>
    <p class="visit-card__doctor">Doctor:Cardiolog</p>
    <p class="visit-card__purpose">Purpose:${elem.dataBody.purpose}</p>
    <p class="visit-card__description">Description${elem.dataBody.description}</p>
    <p class="visit-card__urgency">Urgency:${elem.dataBody.urgency}</p>
    <p class="visit-card__pressure">Pressure:${elem.dataBody.pressure}</p>
    <p class="visit-card__massIndex">MassIndex:${elem.dataBody.massIndex}</p>
    <p class="visit-card__heartIllness">HeartIllness:${elem.dataBody.heartIllness}</p>
    <p class="visit-card__age">Age:${elem.dataBody.age}</p>
    <p class="visit-card__status">Status:${elem.dataBody.status}</p>
    <button class="visit-card__showMore">Show more</button>
    <button class="visit-card__change">Change</button>`
   }
   if (elem.dataBody.doctor ==='dentist'){
       newCard.innerHTML = `        <button class="visit-card__close">X</button>
       <p class="visit-card__fullname">FullName:${elem.dataBody.fullName}</p>
       <p class="visit-card__doctor">Doctor:Dentist</p>
       <p class="visit-card__purpose">Purpose:${elem.dataBody.purpose}</p>
       <p class="visit-card__description">Description:${elem.dataBody.description}</p>
       <p class="visit-card__urgency">Urgency:${elem.dataBody.urgency}</p>
       <p class="visit-card__lastVisitDate">Last visit date:${elem.dataBody.lastVisitDate}</p>
       <p class="visit-card__status">Status:${elem.dataBody.status}</p>
       <button class="visit-card__showMore">Show more</button>
       <button class="visit-card__change">Change</button>`
   }
   if (elem.dataBody.doctor ==="therapist"){
       newCard.innerHTML =`        <button class="visit-card__close">X</button>
       <p class="visit-card__fullname">FullName:${elem.dataBody.fullName}</p>
       <p class="visit-card__doctor">Doctor:Therapist</p>
       <p class="visit-card__purpose">Purpose:${elem.dataBody.purpose}</p>
       <p class="visit-card__description">Description:${elem.dataBody.description}</p>
       <p class="visit-card__urgency">Urgency:${elem.dataBody.urgency}</p>
       <p class="visit-card__age">Age:${elem.dataBody.age}</p>
       <p class="visit-card__status">Status:${elem.dataBody.status}</p>
       <button class="visit-card__showMore">Show more</button>
       <button class="visit-card__change">Change</button>`
   }
    cardsBox.append(newCard)
    return [...document.querySelectorAll('.visit-card')]
   })
}
const verificationButton = document.getElementById('verification');
const loginButton = document.getElementById('login');
const createButton = document.getElementById('create');

verificationButton.addEventListener('click', async (e) => {
    e.preventDefault();

    let emailInput = document.getElementById("email-input").value,
        passwordInput = document.getElementById("password-input").value,
        verificationUser = await User.login(emailInput, passwordInput);

    if (verificationUser) {
        loginButton.style.display = "none";
        createButton.style.display = "block";
        document.querySelector(".btn-secondary").dispatchEvent(new Event("click"));
        getVisits()
    }

})

   
import closeModal from "./closeModal.js"
// import getCardById from "./getCardById.js";
const buttonCreateCard = document.getElementById('createCard');
const removeModal = document.getElementById('removeModal')

document.addEventListener('DOMContentLoaded', () => {

  window.addEventListener('click', e => {
    const target = e.target
    if (!target.closest('#exampleModal')) {
      let select = document.querySelector('#selectDoctor');
      if(select.value !== 'none'){
        removeModal.click()
      }
    }
  })

})

removeModal.addEventListener('click', () => {
  let select = document.querySelector('#selectDoctor');
  let inputs = document.querySelector('.activeInputs')
  inputs.remove()
  select.value = 'none'
})


buttonCreateCard.addEventListener('click', (e) => {
    e.preventDefault()
  let errorsArray = Array.from(document.querySelectorAll('.error'));
  errorsArray.forEach((arrayElement) => {
    arrayElement.style.display = 'none';
  });

  let errors = false;
  let selectDoctor = document.getElementById("selectDoctor");
  let doctor = selectDoctor.value;

  if (doctor !== 'none') {

    let purpose = document.getElementById('purpose').value;
    if (purpose === '') {
      const purposeError = document.getElementById('purposeError')
      purposeError.style.display = 'block'
      errors = true
    }
    let description = document.getElementById('visitDescription').value;
    if (description === '') {
      const visitDescriptionError = document.getElementById('visitDescriptionError')
      visitDescriptionError.style.display = 'block'
      errors = true
    }
    let urgency = document.getElementById('visitUrgency').value;
    if (urgency === 'Select urgency') {
      const visitUrgencyError = document.getElementById('visitUrgencyError')
      visitUrgencyError.style.display = 'block'
      errors = true
    }
    let fullName = document.getElementById('fullName').value;
    if (fullName === '') {
      const fullNameError = document.getElementById('fullNameError')
      fullNameError.style.display = 'block'
      errors = true
    }
    let status = document.getElementById('visitStatus').value;
    

    let doctorClass = new Visit(purpose, description, urgency, fullName, status);
    let dataBody;


    if (doctor === "cardiolog") {

      let pressure = document.getElementById('normalPressure').value;
      if (pressure === '') {
        const normalPressureError = document.getElementById('normalPressureError')
        normalPressureError.style.display = 'block'
        errors = true
      }

      let massIndex = document.getElementById('massIndex').value;
      if (massIndex === '') {
        const massIndexEror = document.getElementById('normalPressureError')
        massIndexEror.style.display = 'block'
        errors = true
      }

      let heartIllness = document.getElementById('heartIllness').value;
      if (heartIllness === '') {
        const heartIllnessError = document.getElementById('heartIllnessError')
        heartIllnessError.style.display = 'block'
        errors = true
      }

      let age = document.getElementById('age').value;
      if (age === '') {
        const ageError = document.getElementById('ageError')
        ageError.style.display = 'block'
        errors = true
      }

      doctorClass = new VisitCardiolog(purpose, description, urgency, fullName, pressure, massIndex, heartIllness, age, status);

    } else if (doctor === "dentist") {

      let lastVisitDate = document.getElementById('lastVisitDate').value;
      if (lastVisitDate === '') {
        const lastVisitDateError = document.getElementById('lastVisitDateError')
        lastVisitDateError.style.display = 'block'
        errors = true
      }
      doctorClass = new VisitDentist(purpose, description, urgency, fullName, lastVisitDate, status);

    } else if (doctor === "therapist") {

      let age = document.getElementById('age').value;
      if (age === '') {
        const ageError = document.getElementById('ageError')
        ageError.style.display = 'block'
        errors = true
      }
      doctorClass = new VisitTherapist(purpose, description, urgency, fullName, age, status);
    }

    dataBody = doctorClass.getDataJson();
    // localStorage.setItem('token', '64c0d805-762f-48fe-8302-576aa8a76686')

    if (!errors) {
      let newCard = document.createElement('div')
      newCard.setAttribute('data-card-id','REAL_ID_FROM_CARD')
      newCard.setAttribute('class','visit-card')
      fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.token}`
        },
        body: JSON.stringify({ dataBody })
      })
        .then(response => response.json())
        .then(response => {
        newCard.setAttribute('id',`${response.id}`)
        newCard.innerHTML = doctorClass.render(response.id)
      } 
  )
        cardsBox.append(newCard)
        getVisits()
        closeModal()
    }

  }

});

async function deletePost(id){
  await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
  method: 'DELETE',
  headers: {
    'Authorization': `Bearer ${localStorage.token}`
  },
}).then(async response => {
  if (response.ok){
   await getVisits()
  }
})
}
document.addEventListener('click', async (e)=>{
  if (e.target.classList.contains('visit-card__close')){
    const currentWrapperId = Number(e.target.closest('.visit-card').getAttribute('data-card-id'))
    deletePost(currentWrapperId)
  }
})
// filter
  // let formFilter = document.getElementById('formFilter') раньше стоял такой формфильтер
//   let formFilter = document.querySelector('.filter__button')
//   let boxCardsNodeList = document.querySelectorAll(".visit-card");
//   let boxHtmlCards = Array.from(boxCardsNodeList); 
  // let cardsList = getVisits;
  // todo USE ARRAY FROM FETCH
// раньше стоял лисенер по клику
//   formFilter.addEventListener('click', (evt)=> {
//       evt.preventDefault();
      
//       let searchTextVal= document.getElementById('searchText').value.trim();
//       let urgencyInputVal = document.getElementById('urgencyInput').value;
//       let statusInputVal = document.getElementById('statusInput').value;
      // filtering cards
//       const filteredCards = filterAllForm(cardsList, statusInputVal, urgencyInputVal, searchTextVal)

      // hide all cards
//       boxHtmlCards.forEach( card =>{
//           card.classList.add('hide')
//       })

      // show filtered cards
//       filteredCards.forEach(card=>{
//           const el = document.querySelector(`[data-card-id = '${card.id}']`)
//           el.classList.remove('hide');
//       })
// })

