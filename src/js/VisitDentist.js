import Visit from './Visit.js';
class VisitDentist extends Visit {
  constructor(purpose, description, urgency, fullName, lastVisitDate, status) {
    super(purpose, description, urgency, fullName, status);
    this.lastVisitDate = lastVisitDate;
  }



  setLastVisitDate(lastVisitDate) {
    this.lastVisitDate = lastVisitDate;
  }
  getLastVisitDate() {
    return this.lastVisitDate;
  }
  render(){
    return `
    <button class="visit-card__close">X</button>
    <p class="visit-card__fullname">FullName: ${this.fullName}</p>
    <p class="visit-card__doctor">Doctor: Dentist</p>
    <button class="visit-card__showMore">Show more</button>
    <button class="visit-card__change">Change</button>
`
  }

  getDataJson() {

    return {
      purpose: this.purpose,
      description: this.description,
      urgency: this.urgency,
      fullName: this.fullName,
      lastVisitDate: this.lastVisitDate,
      status: this.status,
      doctor: 'dentist'
    };

  }
}

export default VisitDentist;
