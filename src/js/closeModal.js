const modal = document.querySelector('.submitCreation')
const closeModalBtn = document.querySelector('#createCard')
const closeModal = function(){
    modal.classList.remove('show')
    modal.style.display = 'none'
    modal.setAttribute('aria-hidden','true')
    modal.removeAttribute('aria-modal')
    modal.removeAttribute('role')
    document.body.removeAttribute('style')
    document.body.removeAttribute('class')
    const modalBG = document.querySelector('.modal-backdrop')
    modalBG.remove()
}
export default closeModal